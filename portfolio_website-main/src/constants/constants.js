export const projects = [
  {
    title: 'Pawstive Outcome',
    description: "Our project is for the employees of a dog adoption organization called Pawsitive Outcome to keep track of the dogs available for adoption, and if adopted, file their information along with their adopter's information into a database.",
      image: '/images/1.png',
      tags: ['FastAPI', 'Redux', 'React'],
    source: 'https://gitlab.com/whisker-watchers/pawsitive-outcome',
    visit: 'https://whisker-watchers.gitlab.io/pawsitive-outcome/Login',
    id: 0,
  },
  {
    title: 'Car-Car',
    description:"While building it you're going to learn many advanced React & JavaScript topics, as well as how to use Stripe for card transactions. On top of that, at the end of the video, you will have this unique and complex webshop app that you will be able to add to your portfolio. And trust me, e-commerce applications are impressive.",
    image: '/images/2.png',
    tags: ['React','Django', 'JavaScript'],
    source: 'https://google.com',
    visit: 'https://google.com',
    id: 1,
  },
  {
    title: 'WebRTC App',
    description: "This is a code repository for the corresponding YouTube video. In this tutorial, we're going to build and deploy a React Video Chat Application using WebRTC.",
      image: '/images/3.jpg',
      tags: ['React', 'WebRTC'],
    source: 'https://google.com',
    visit: 'https://google.com',
    id: 2,
  },
  {
    title: 'Unichat',
    description: "This is a code repository for the corresponding video tutorial. In this video, we will create a full Realtime Chat Application",
    image: '/images/4.jpg',
    tags: ['React', 'ChatEngine', 'Firebase'],
    source: 'https://google.com',
    visit: 'https://google.com',
    id: 3,
  },
];

export const TimeLineData = [
  // { year: 2017, text: 'Started my journey', },
  // { year: 2018, text: 'Worked as a freelance developer', },
  // { year: 2019, text: 'Founded JavaScript Mastery', },
  // { year: 2020, text: 'Shared my projects with the world', },
  { year: 2023, text: 'Graduated Hack Reactor and Ready to be Hired', },
];
