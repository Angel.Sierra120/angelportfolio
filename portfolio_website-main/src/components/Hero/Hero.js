import React from 'react';

import { Section, SectionText, SectionTitle } from '../../styles/GlobalComponents';
import Button from '../../styles/GlobalComponents/Button';
import { LeftSection } from './HeroStyles';

const Hero = (props) => (
  <>
    <Section row nopadding>
      <LeftSection>
        <SectionTitle main center>
          Hey, I'm Angel <br />
          Full-Stack Developer
        </SectionTitle>
        <SectionText>
        Welcome to my portfolio page. Here are the highlights of my software development journey so far. I am a recent Hack Reactor graduate, my projects include both student projects and personal projects.         </SectionText>
        <Button onClick={props.handleClick}>Learn More</Button>
      </LeftSection>
    </Section>
  </>
);

export default Hero;
